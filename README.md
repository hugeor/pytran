# README #

This README would normally document whatever steps are necessary to get __UWHPSC (Coursera Ed)__ homework done.

### What is this repository for? ###

* Learn Git 
* Learn Python 
* Learn Fortran 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Go to [software intro](http://faculty.washington.edu/rjl/uwhpsc-coursera/software_installation.html) and fix your own environment.

### Contribution guidelines ###

* I am using Fedora (this should not matter).
* Coursera ed 2014.
* Don't just copy. Try it for youself!
